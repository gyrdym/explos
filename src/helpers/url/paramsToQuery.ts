export function paramsToQuery(
    params: Record<string, string>,
    {
        leading = true,
        encode = true,
    } = {},
) {
    return Object
        .entries(params)
        .reduce((query, [key, value]) => ''
            + `${query}`
            + `${query 
                ? '&' 
                : leading ? '?' : ''}`
            + `${key}=${encode ? encodeURI(value) : value}`,
        '');
}
