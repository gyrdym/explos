import {paramsToQuery} from './paramsToQuery';

export function buildUrl({
    protocol = 'https',
    domain,
    paths = [],
    params = {},
    encodeParams = true,
}: {
    protocol?: 'https'|'http',
    domain: string,
    paths?: string[],
    params?: Record<string, string>,
    encodeParams?: boolean,
}) {
    return `${protocol}://${domain}/${paths
        .join('/')
        .concat(
            paramsToQuery(
                params, {
                    encode: encodeParams
                },
            ),
        )}`;
}
