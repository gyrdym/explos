import {INormalizedData} from '../../interfaces/utility/normalizedData';

export function normalizeArray<T, I extends string>(
    array: T[],
    getId: (item: T) => I,
) {
    return array
        .reduce<INormalizedData<T>>((normalized, item) => {
            const id = getId(item);

            return {
                ...normalized,
                byId: {
                    ...normalized.byId,
                    [id]: item,
                },
                ids: [
                    ...normalized.ids,
                    id,
                ],
            };
        }, {byId: {}, ids: []});
}
