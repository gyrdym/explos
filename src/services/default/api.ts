import { IApi } from '../../interfaces/services/api';
import { ICompany } from '../../interfaces/domain/company';
import { ISector } from '../../interfaces/domain/sector';
import { ISymbol } from '../../interfaces/domain/symbol';
import { injectable } from 'inversify';
import { IQuoteResponse } from '../../interfaces/domain/quoteResponse';
import { buildUrl } from '../../helpers/url/buildUrl';

@injectable()
export class Api implements IApi {
  private readonly domain = `${process.env.REACT_APP_API_BASE_PATH}.iexapis.com`;

  getCompany(ticker: string): Promise<ICompany> {
    const url = buildUrl({
      domain: this.domain,
      paths: [...this.baseStocksUrlPaths, ticker, 'company'],
      params: this.baseParams,
    });

    return fetch(url)
      .then((response) => response.json())
      .catch((error) => console.error(`Api.getCompany error, ${error}`));
  }

  getSectorQuotes(sectorName: string): Promise<IQuoteResponse[]> {
    console.log('sectorName', sectorName);
    const url = buildUrl({
      domain: this.domain,
      paths: [...this.baseStocksUrlPaths, 'market', 'collection', 'sector'],
      params: {
        ...this.baseParams,
        collectionName: sectorName,
      },
    });

    return fetch(url)
      .then((response) => response.json())
      .catch((error) => console.error(`Api.getSectorQuotes error, ${error}`));
  }

  getSectors(): Promise<ISector[]> {
    const url = buildUrl({
      domain: this.domain,
      paths: [...this.baseRefDataUrlPaths, 'sectors'],
      params: this.baseParams,
    });

    return fetch(url)
      .then((response) => response.json())
      .catch((error) => console.error(`Api.getSectors error, ${error}`));
  }

  getSymbols(): Promise<ISymbol[]> {
    const url = buildUrl({
      domain: this.domain,
      paths: [...this.baseRefDataUrlPaths, 'symbols'],
      params: this.baseParams,
    });

    return fetch(url)
      .then((response) => response.json())
      .catch((error) => console.error(`Api.getSymbols error, ${error}`));
  }

  private get baseUrlPaths() {
    return ['stable'];
  }

  private get baseRefDataUrlPaths() {
    return [...this.baseUrlPaths, 'ref-data'];
  }

  private get baseStocksUrlPaths() {
    return [...this.baseUrlPaths, 'stock'];
  }

  private get baseParams() {
    return {
      token: `${process.env.REACT_APP_API_TOKEN}`,
    };
  }
}
