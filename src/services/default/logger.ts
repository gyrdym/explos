import {Consola} from 'consola';
import {decorate, injectable} from 'inversify';

decorate(injectable(), Consola);

@injectable()
export class Logger extends Consola {}
