import {IQuotesState} from './quotes.interface';

export const quotesInitialState: IQuotesState = {
    byId: {},
    ids: [],
};
