import { IApi } from '../../interfaces/services/api';
import { ILogger } from '../../interfaces/services/logger';
import { ioc } from '../../ioc/ioc';
import { iocTypes } from '../../ioc/iocTypes';
import { IDispatch } from '../store';
import { quotesSlice } from './quotes.slice';

export const loadQuotes = (sector: string) => async (dispatch: IDispatch) => {
  try {
    const quotes = await ioc.get<IApi>(iocTypes.API).getSectorQuotes(sector);

    dispatch(quotesSlice.actions.setQuotes({ quotes, sector }));
  } catch (error) {
    ioc
      .get<ILogger>(iocTypes.LOGGER)
      .error(`Error while loading quotes for the sector "${sector}":`, error);
  }
};
