import { IStore } from '../store';

export const quotesSelector = (state: IStore) =>
  state.quotes.ids.map((id) => state.quotes.byId[id]);
