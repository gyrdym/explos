import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {quotesInitialState} from './quotes.constants';
import {normalizeArray} from '../../helpers/grouping/normalizeArray';
import {IQuoteResponse} from '../../interfaces/domain/quoteResponse';

export const quotesSlice = createSlice({
    name: 'quotes',
    initialState: quotesInitialState,
    reducers: {
        setQuotes(state, action: PayloadAction<{quotes: IQuoteResponse[], sector: string}>) {
            const {byId, ids} = normalizeArray(
                action
                    .payload
                    .quotes
                    .map((quote) => ({
                        ...quote,
                        sector: action.payload.sector,
                    })),
                (quote) => quote.symbol,
            );

            state.byId = byId;
            state.ids = ids;
        },
    },
});
