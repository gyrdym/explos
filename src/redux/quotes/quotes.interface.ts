import {IQuote} from '../../interfaces/domain/quote';
import {INormalizedData} from '../../interfaces/utility/normalizedData';

export type IQuotesState = INormalizedData<IQuote>;
