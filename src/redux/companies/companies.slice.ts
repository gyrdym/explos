import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {companiesInitialState} from './companies.constants';
import {ICompany} from '../../interfaces/domain/company';

export const companiesSlice = createSlice({
    name: 'companies',
    initialState: companiesInitialState,
    reducers: {
        setCompany(state, action: PayloadAction<ICompany>) {
            state.byId[action.payload.symbol] = action.payload;
            state.ids.push(action.payload.symbol);
        },
    },
});
