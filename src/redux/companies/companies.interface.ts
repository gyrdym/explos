import {ICompany} from '../../interfaces/domain/company';
import {INormalizedData} from '../../interfaces/utility/normalizedData';

export type ICompaniesState = INormalizedData<ICompany>;
