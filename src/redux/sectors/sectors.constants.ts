import {ISectorsState} from './sectors.interface';

export const sectorsInitialState: ISectorsState = {
    sectorNames: [],
    selectedSector: '',
};
