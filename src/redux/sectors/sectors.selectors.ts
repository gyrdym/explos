import {IStore} from '../store';

export const sectorsSelector = (state: IStore) => ({
    sectorNames: state.sectors.sectorNames,
    selectedSectorName: state.sectors.selectedSector,
});
