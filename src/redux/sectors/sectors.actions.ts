import { IApi } from '../../interfaces/services/api';
import { ILogger } from '../../interfaces/services/logger';
import { ioc } from '../../ioc/ioc';
import { iocTypes } from '../../ioc/iocTypes';
import { IDispatch } from '../store';
import { sectorsSlice } from './sectors.slice';

export const loadSectors = () => async (dispatch: IDispatch) => {
  try {
    const sectors = await ioc.get<IApi>(iocTypes.API).getSectors();

    dispatch(sectorsSlice.actions.setSectors(sectors));
  } catch (error) {
    ioc
      .get<ILogger>(iocTypes.LOGGER)
      .error('Error while loading sectors: ', error);
  }
};
