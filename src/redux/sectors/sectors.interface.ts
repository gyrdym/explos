export interface ISectorsState {
    sectorNames: string[];
    selectedSector: string;
}
