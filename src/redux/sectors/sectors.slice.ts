import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {sectorsInitialState} from './sectors.constants';
import {ISector} from '../../interfaces/domain/sector';

export const sectorsSlice = createSlice({
    name: 'sectors',
    initialState: sectorsInitialState,
    reducers: {
        setSectors: (state, action: PayloadAction<ISector[]>) => {
            state.sectorNames = action
                .payload
                .map(({name}) => name);
        },
        selectSector: (state, action: PayloadAction<string>) => {
            state.selectedSector = action.payload;
        },
    },
});
