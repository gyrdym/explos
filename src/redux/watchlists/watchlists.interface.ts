import {IWatchlist} from '../../interfaces/domain/watchlist';
import {INormalizedData} from '../../interfaces/utility/normalizedData';

export type IWatchlistsState = INormalizedData<IWatchlist>;
