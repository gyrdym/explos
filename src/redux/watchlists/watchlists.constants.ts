import {IWatchlistsState} from './watchlists.interface';

export const watchlistsInitialState: IWatchlistsState = {
    byId: {},
    ids: [],
};
