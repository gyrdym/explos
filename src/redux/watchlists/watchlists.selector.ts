import {IStore} from '../store';

export const watchlistsSelector = (state: IStore) => ({
    watchlists: state.watchlists.ids.map((id) => state.watchlists.byId[id]),
});
