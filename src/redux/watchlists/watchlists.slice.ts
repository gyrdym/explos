import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {watchlistsInitialState} from './watchlists.constants';
import {IWatchlist} from '../../interfaces/domain/watchlist';

export const watchlistsSlice = createSlice({
    name: 'watchlists',
    initialState: watchlistsInitialState,
    reducers: {
        add: (state, action: PayloadAction<IWatchlist>) => {
            state.byId = {
                ...state.byId,
                [action.payload.name]: {
                    ...state.byId[action.payload.name],
                    ...action.payload,
                },
            };

            if (state.ids.includes(action.payload.name)) {
                return;
            }

            state.ids.push(action.payload.name);
        },
    },
});
