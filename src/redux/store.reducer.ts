import {combineReducers} from 'redux';
import {investmentsSlice} from './investments/investments.slice';
import {symbolsSlice} from './symbols/symbols.slice';
import {sectorsSlice} from './sectors/sectors.slice';
import {companiesSlice} from './companies/companies.slice';
import {quotesSlice} from './quotes/quotes.slice';
import {watchlistsSlice} from './watchlists/watchlists.slice';

export const storeReducer = combineReducers({
    investments: investmentsSlice.reducer,
    symbols: symbolsSlice.reducer,
    sectors: sectorsSlice.reducer,
    companies: companiesSlice.reducer,
    quotes: quotesSlice.reducer,
    watchlists: watchlistsSlice.reducer,
});
