import {IInvestment} from '../../interfaces/features/investment';
import {INormalizedData} from '../../interfaces/utility/normalizedData';

export type IInvestmentsState = INormalizedData<IInvestment>;
