import {IInvestmentsState} from './investments.interface';

export const investmentsInitialState: IInvestmentsState = {
    byId: {},
    ids: [],
};
