import {IStore} from '../store';

export const investmentsSelector = (state: IStore) => ({
    investments: state
        .investments
        .ids
        .map((id) => state
            .investments
            .byId[id])
});
