import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {investmentsInitialState} from './investments.constants';
import {IInvestment} from '../../interfaces/features/investment';

export const investmentsSlice = createSlice({
    name: 'investments',
    initialState: investmentsInitialState,
    reducers: {
        add: (state, action: PayloadAction<IInvestment>) => {
            state.byId = {
                ...state.byId,
                [action.payload.id]: action.payload,
            };
            state.ids = [
                ...state.ids,
                action.payload.id,
            ];
        },
    },
});
