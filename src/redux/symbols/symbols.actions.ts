import {symbolsSlice} from './symbols.slice';
import {ioc} from '../../ioc/ioc';
import {IApi} from '../../interfaces/services/api';
import {iocTypes} from '../../ioc/iocTypes';
import {IDispatch} from '../store';
import {ILogger} from '../../interfaces/services/logger';

export const loadSymbolsAction = async (dispatch: IDispatch) => {
    try {
        const symbols = await ioc
            .get<IApi>(iocTypes.API)
            .getSymbols();

        dispatch(symbolsSlice.actions.setSymbols(symbols));
    } catch (error) {
        ioc
            .get<ILogger>(iocTypes.LOGGER)
            .error('Error while loading symbols: ', error);

        dispatch(symbolsSlice.actions.symbolsLoadingFailed());
    }
};
