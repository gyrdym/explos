import {ISymbol} from '../../interfaces/domain/symbol';
import {INormalizedData} from '../../interfaces/utility/normalizedData';

export type ISymbolsState = INormalizedData<ISymbol>
    & {selectedTicker?: string};
