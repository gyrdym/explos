import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {symbolsInitialState} from './symbols.constants';
import {ISymbol} from '../../interfaces/domain/symbol';
import {normalizeArray} from '../../helpers/grouping/normalizeArray';

export const symbolsSlice = createSlice({
    name: 'tickers',
    initialState: symbolsInitialState,
    reducers: {
        setSymbols: (state, action: PayloadAction<ISymbol[]>) => {
            const {byId, ids} = normalizeArray(action.payload, (symbol) => symbol.symbol);

            state.byId = byId;
            state.ids = ids;
        },
        selectTicker: (state, action: PayloadAction<string>) => {
            state.selectedTicker = action.payload;
        },
        symbolsLoadingFailed: (state) => {},
    },
});
