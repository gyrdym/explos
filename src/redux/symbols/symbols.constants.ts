import {ISymbolsState} from './symbols.interface';

export const symbolsInitialState: ISymbolsState = {
    byId: {},
    ids: [],
};
