import {IStore} from '../store';

export const symbolsSelector = (state: IStore) => ({
    selectedTicker: state.symbols.selectedTicker,
    tickers: state.symbols.ids,
});
