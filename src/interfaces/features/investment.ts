export interface IInvestment {
    readonly id: string;
    readonly date: Date;
    readonly ticker: string;
    readonly amount: number;
    readonly reason: string;
}
