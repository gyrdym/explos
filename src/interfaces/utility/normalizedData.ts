export interface INormalizedData<T> {
    byId: Record<string, T>;
    ids: string[];
}
