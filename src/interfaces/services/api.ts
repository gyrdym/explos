import {ICompany} from '../domain/company';
import {ISector} from '../domain/sector';
import {ISymbol} from '../domain/symbol';
import {IQuoteResponse} from '../domain/quoteResponse';

export interface IApi {
    getSectors(): Promise<ISector[]>;
    getSymbols(): Promise<ISymbol[]>;
    getCompany(ticker: string): Promise<ICompany>;
    getSectorQuotes(sectorName: string): Promise<IQuoteResponse[]>;
}
