export enum PriceType {
    Tops = 'tops',
    Sip = 'sip',
    PreviousClose = 'previousclose',
    Close = 'close',
    IexLastTrade = 'iexlasttrade',
}
