export enum IssueType {
    /**
     * American Depository Receipt (ADR’s)
     */
    AD = 'ad',

    /**
     * GDR
     */
    GDR = 'gdr',

    /**
     * Real Estate Investment Trust (REIT’s)
     */
    RE = 're',

    /**
     * Closed end fund (Stock and Bond Fund)
     */
    CE = 'ce',

    /**
     * Secondary Issue
     */
    SI = 'si',

    /**
     * Limited Partnerships
     */
    LP = 'lp',

    /**
     * Common Stock
     */
    CS = 'cs',

    /**
     * Exchange Traded Fund (ETF)
     */
    ET = 'et',

    /**
     * Warrant
     */
    WT = 'wt',

    /**
     * Right
     */
    RT = 'rt',

    /**
     * Open Ended Fund
     */
    OEF = 'oef',

    /**
     * Closed Ended Fund
     */
    CEF = 'cef',

    /**
     * Preferred Stock
     */
    PS = 'ps',

    /**
     * Unit
     */
    UT = 'ut',

    /**
     * Structured Product
     */
    Struct = 'struct',

    /**
     * Temporary
     */
    Temp = 'temp',
}
