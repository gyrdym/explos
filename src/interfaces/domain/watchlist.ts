import {ISymbol} from './symbol';

export interface IWatchlist {
    readonly name: string;
    readonly symbols: ISymbol[];
}
