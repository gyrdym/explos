import {PriceType} from './priceType';
import {PriceSourceType} from './priceSourceType';

export interface IQuoteResponse {
    /**
     * Refers to the stock ticker
     */
    readonly symbol: string;

    /**
     * Refers to the company name
     */
    readonly companyName: string;

    /**
     * Refers to the primary listing exchange for the symbol
     */
    readonly primaryExchange: string;

    /**
     * Refers to the source of the latest price
     */
    readonly calculationPrice: PriceType;

    /**
     * Refers to the official open price from the SIP. 15 minute delayed (can be null after 00:00 ET, before 9:45 and
     * weekends)
     */
    readonly open: number;

    /**
     * Refers to the official listing exchange time for the open from the SIP. 15 minute delayed
     */
    readonly openTime: number;

    /**
     * Source of open if available
     */
    readonly openSource: string;

    /**
     * Refers to the 15-minute delayed official close price from the SIP. For Nasdaq-listed stocks, if you do not have
     * UTP authorization, between 4:00 p.m. and 8 p.m. E.T. this field will return the price of the last trade on IEX
     * rather than the SIP closing price.
     */
    readonly close: number;

    /**
     * Refers to the official listing exchange time for the close from the SIP. 15 minute delayed
     */
    readonly closeTime: number;

    /**
     * Source of close if available
     */
    readonly closeSource: string;

    /**
     * Refers to the market-wide highest price from the SIP. 15 minute delayed during normal market hours 9:30 - 16:00
     * (null before 9:45 and weekends).
     */
    readonly high: number;

    /**
     * Refers to time high was updated as epoch timestamp
     */
    readonly highTime: number;

    /**
     * This will represent a human readable description of the source of high.
     */
    readonly highSource: PriceSourceType;

    /**
     * Refers to the market-wide lowest price from the SIP. 15 minute delayed during normal market hours 9:30 - 16:00
     * (null before 9:45 and weekends).
     */
    readonly low: number;

    readonly latestPrice: number;

    // TODO: add the rest of the domain fields
}
