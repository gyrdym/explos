import {IssueType} from './issueType';

export interface ISymbol {
    /**
     * Refers to the symbol represented in Nasdaq Integrated symbology (INET).
     */
    readonly symbol: string;

    /**
     * Refers to Exchange using https://sandbox.iexapis.com/stable/ref-data/exchanges?token=Tsk_59ed502ceebd4c7197bfb4dd1c78ddb3
     */
    readonly exchange: string;

    /**
     * Refers to the name of the company or security.
     */
    readonly name: string;

    /**
     * Refers to the date the symbol reference data was generated.
     */
    readonly date: string;

    /**
     * Will be true if the symbol is enabled for trading on IEX.
     */
    readonly isEnabled: boolean;

    /**
     * Refers to the common issue type
     */
    readonly type: IssueType;

    /**
     * Refers to the country code for the symbol using ISO 3166-1 alpha-2
     */
    readonly region: string;

    /**
     * Refers to the currency the symbol is traded in using ISO 4217
     */
    readonly currency: string;

    /**
     * 	Unique ID applied by IEX to track securities through symbol changes.
     */
    readonly iexId: string;

    /**
     * OpenFIGI id for the security if available
     */
    readonly figi: string;

    /**
     * CIK number for the security if available
     */
    readonly cik: string;
}
