import {IssueType} from './issueType';

export interface ICompany {
    /**
     * Ticker of the company
     */
    readonly symbol: string;

    /**
     * Name of the company
     */
    readonly companyName: string;

    /**
     * Number of employees
     */
    readonly employees: number;

    /**
     * Refers to Exchange using https://sandbox.iexapis.com/stable/ref-data/exchanges?token=Tsk_59ed502ceebd4c7197bfb4dd1c78ddb3
     */
    readonly exchange: string;

    /**
     * Refers to the industry the company belongs to
     */
    readonly industry: string;

    /**
     * Website of the company
     */
    readonly website: string;

    /**
     * Description for the company
     */
    readonly description: string;

    /**
     * Name of the CEO of the company
     */
    readonly CEO: string;

    /**
     * Name of the security
     */
    readonly securityName: string;

    /**
     * Refers to the common issue type of the stock.
     */
    readonly issueType: IssueType;

    /**
     * Refers to the sector the company belongs to
     */
    readonly sector: string;

    /**
     * Primary SIC Code for the symbol (if available)
     */
    readonly primarySicCode: string;

    /**
     * An array of strings used to classify the company.
     */
    readonly tags: string[];

    /**
     * Street address of the company if available
     */
    readonly address: string;

    /**
     * Street address of the company if available
     */
    readonly address2: string;

    /**
     * State of the company if available
     */
    readonly state: string;

    /**
     * City of the company if available
     */
    readonly city: string;

    /**
     * Zip code of the company if available
     */
    readonly zip: string;

    /**
     * Country of the company if available
     */
    readonly country: string;

    /**
     * Phone number of the company if available
     */
    readonly phone: string;
}
