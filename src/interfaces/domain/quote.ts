import {IQuoteResponse} from './quoteResponse';

export interface IQuote extends IQuoteResponse {
    sector: string;
}
