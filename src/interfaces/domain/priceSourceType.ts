export enum PriceSourceType {
    IexRealTime = 'IEX real time price',
    FifteenMinutesDelayed = '15 minute delayed price',
    Close = 'Close',
    PreviousClose = 'Previous close',
}
