import { TextField } from '@material-ui/core';
import React from 'react';
import { QuoteTable } from '../quoteTable';
import { SectorMenu } from '../sectorMenu';
import './App.css';

function App() {
  return (
    <div className="App">
      <TextField />
      <SectorMenu />
      <QuoteTable />
    </div>
  );
}

export default App;
