import {Button, Modal} from '@material-ui/core';
import {useAppDispatch} from '../../redux/store';
import {default as React, useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import {loadSectors} from '../../redux/sectors/sectors.actions';
import {sectorsSelector} from 'src/redux/sectors/sectors.selectors';
import {loadQuotes} from '../../redux/quotes/quotes.actions';
import {useInvestmentsFormStyle} from './investmentsForm.style';
import {InvestmentsSymbolMenu} from '../investmentsSymbolMenu/investmentsSymbolMenu';

export function InvestmentsForm() {
    const [isOpen, setIsOpen] = useState(false);
    const dispatch = useAppDispatch();
    const {sectorNames, selectedSectorName} = useSelector(sectorsSelector);
    const classes = useInvestmentsFormStyle();
    const handleOpen = () => {
        setIsOpen(true);
    };
    const handleClose = () => {
        setIsOpen(false);
    };

    useEffect(() => {
        if (!isOpen || sectorNames.length > 0) {
            return;
        }
        dispatch(loadSectors);
    }, [sectorNames, isOpen]);

    useEffect(() => {
        if (!selectedSectorName) {
            return;
        }
        dispatch(loadQuotes(selectedSectorName));
    }, [selectedSectorName]);

    return (
        <div className={classes.root}>
            <Modal
                className={classes.modal}
                open={isOpen}
                onClose={handleClose}
            >
                <div className={classes.paper}>
                    <InvestmentsSymbolMenu />
                </div>
            </Modal>
            <Button
                variant="contained"
                color="primary"
                onClick={handleOpen}
            >
                Add record
            </Button>
        </div>
    );
}
