import {default as React} from 'react';
import {useAppDispatch} from '../../redux/store';
import {Dropdown} from '../common/dropdown/dropdown';
import {symbolsSlice} from '../../redux/symbols/symbols.slice';
import {useSelector} from 'react-redux';
import {symbolsSelector} from '../../redux/symbols/symbols.selector';

export function InvestmentsSymbolMenu() {
    const dispatch = useAppDispatch();
    const {selectedTicker} = useSelector(symbolsSelector);
    const handleClose = (name: string) => {
        if (!name) {
            return;
        }

        dispatch(symbolsSlice.actions.selectTicker(name));
    };

    return (
        <Dropdown
            selectedOption={selectedTicker}
            placeholder={'Выбрать символ'}
            onClose={handleClose}
            options={[]}
        />
    );
}
