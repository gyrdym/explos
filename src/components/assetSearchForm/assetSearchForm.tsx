import {Modal} from '@material-ui/core';
import {default as React} from 'react';
import {useAssetSearchFormStyle} from './assetSearchForm.style';
import {PERatioBlock} from './peRatioBlock';
import {DERatioBlock} from './deRatioBlock';
import {NetIncomeBlock} from './netIncomeBlock';
import {DividendsBlock} from './dividendsBlock';

export function AssetSearchForm({
    isOpen = false,
    handleClose = () => {},
}: {
    isOpen?: boolean,
    handleClose?: () => void,
} = {}) {
    const classes = useAssetSearchFormStyle();

    return (
        <Modal
            className={classes.modal}
            open={isOpen}
            onClose={handleClose}
        >
            <div className={classes.paper}>
                <PERatioBlock />
                <DERatioBlock />
                <NetIncomeBlock />
                <DividendsBlock />
            </div>
        </Modal>
    );
}
