import {TextField, Typography} from '@material-ui/core';
import {default as React} from 'react';
import {useAssetSearchFormStyle} from './assetSearchForm.style';

export function DERatioBlock() {
    const classes = useAssetSearchFormStyle();

    return (
        <div className={classes.block}>
            <Typography variant="h6">
                DE ratio
            </Typography>
            <div className={classes.radioGroupBlock}>
                <Typography
                    className={classes.radioGroupBlockCaption}
                    variant="caption"
                >
                    Less than or equal to
                </Typography>
                <TextField
                    className={classes.textfield}
                    type="number"
                    value={1}
                />
            </div>
        </div>
    );
}
