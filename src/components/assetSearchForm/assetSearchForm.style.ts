import {createStyles, makeStyles} from '@material-ui/core';

export const useAssetSearchFormStyle = makeStyles((theme) => createStyles({
    root: {
        height: 300,
        flexGrow: 1,
        minWidth: 300,
        transform: 'translateZ(0)',
        // The position fixed scoping doesn't work in IE 11.
        // Disable this demo to preserve the others.
        '@media all and (-ms-high-contrast: none)': {
            display: 'none',
        },
    },
    modal: {
        display: 'flex',
        padding: theme.spacing(1),
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        width: 600,
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    block: {
        marginBottom: 25,
    },
    radioGroup: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    radioGroupBlock: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    radioGroupLabel: {
        marginRight: 0,
    },
    radioGroupBlockCaption: {
        marginRight: 10,
    },
    textfield: {
        width: 50,
        marginRight: 20,
    },
}));
