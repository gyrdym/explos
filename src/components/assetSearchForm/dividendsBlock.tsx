import {TextField, Typography} from '@material-ui/core';
import {default as React} from 'react';
import {useAssetSearchFormStyle} from './assetSearchForm.style';

export function DividendsBlock() {
    const classes = useAssetSearchFormStyle();

    return (
        <div className={classes.block}>
            <Typography variant="h6">
                Dividends
            </Typography>
            <div className={classes.radioGroupBlock}>
                <Typography
                    className={classes.radioGroupBlockCaption}
                    variant="caption"
                >
                    Have been paid for the last
                </Typography>
                <TextField
                    className={classes.textfield}
                    type="number"
                    value={10}
                />
                <Typography
                    className={classes.radioGroupBlockCaption}
                    variant="caption"
                >
                    years
                </Typography>
            </div>
        </div>
    );
}
