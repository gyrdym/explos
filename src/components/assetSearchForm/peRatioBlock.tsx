import {FormControlLabel, Radio, RadioGroup, TextField, Typography} from '@material-ui/core';
import {default as React} from 'react';
import {useAssetSearchFormStyle} from './assetSearchForm.style';

export function PERatioBlock() {
    const classes = useAssetSearchFormStyle();
    const name = 'peRatio';

    return (
        <div className={classes.block}>
            <Typography variant="h6">
                PE ratio
            </Typography>
            <RadioGroup
                name={name}
                className={classes.radioGroup}
            >
                <Typography
                    className={classes.radioGroupBlockCaption}
                    variant="caption"
                >
                    Less than or equal to
                </Typography>
                <div className={classes.radioGroupBlock}>
                    <FormControlLabel
                        value="custom"
                        control={<Radio name={name} />}
                        className={classes.radioGroupLabel}
                        label=""
                    />
                    <TextField
                        className={classes.textfield}
                        type="number"
                        value={15}
                    />
                </div>
                <div className={classes.radioGroupBlock}>
                    <FormControlLabel
                        value="average"
                        control={<Radio
                            name={name}
                            checked={true}
                        />}
                        className={classes.radioGroupLabel}
                        label=""
                    />
                    <Typography
                        className={classes.radioGroupBlockCaption}
                        variant="caption"
                    >
                        the sector average value
                    </Typography>
                </div>
            </RadioGroup>
        </div>
    );
}
