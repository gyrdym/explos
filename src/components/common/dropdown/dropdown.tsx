import { Menu, MenuItem } from '@material-ui/core';
import { default as React, useRef, useState } from 'react';

export function Dropdown({
  selectedOption,
  placeholder,
  options,
  onClose,
  onOpen,
}: {
  selectedOption?: string;
  placeholder?: string;
  options: string[];
  onClose?: (name: string) => void;
  onOpen?: () => void;
}) {
  const [isOpen, setIsOpen] = useState(false);
  const anchorRef = useRef(null);
  const handleOpen = () => {
    onOpen?.();
    setIsOpen(true);
  };
  const handleClose = (name: string) => {
    onClose?.(name);
    setIsOpen(false);
  };

  return (
    <>
      <div onClick={handleOpen} ref={anchorRef}>
        {selectedOption || placeholder}
      </div>
      <Menu
        anchorEl={anchorRef.current}
        keepMounted
        open={isOpen}
        onClose={handleClose}
      >
        {options.map((option) => (
          <MenuItem
            key={option}
            selected={selectedOption === option}
            onClick={() => handleClose(option)}
          >
            {option}
          </MenuItem>
        ))}
      </Menu>
    </>
  );
}
