import clsx from 'clsx';
import * as React from 'react';
import { useCallback } from 'react';
import { AutoSizer, Column, Table } from 'react-virtualized';
import { IVirtualizedTableProps } from './VirtualizedTable.interface';
import { useVirtualizedTableStyles } from './VirtualizedTable.style';
import { VirtualizedTableCell } from './VirtualizedTableCell';
import { VirtualizedTableHeader } from './VirtualizedTableHeader';

export function VirtualizedTable<T>({
  columns,
  rowHeight = 48,
  headerHeight = 48,
  onRowClick,
  rowCount,
  rowGetter,
}: IVirtualizedTableProps<T>) {
  const classes = useVirtualizedTableStyles();

  const getRowClassName = useCallback(({ index }: { index: number }) => {
    return clsx(classes.tableRow, classes.flexContainer, {
      [classes.tableRowHover]: index !== -1 && onRowClick != null,
    });
  }, []);

  return (
    <AutoSizer>
      {({ height, width }) => (
        <Table
          height={height}
          width={width}
          rowHeight={rowHeight}
          gridStyle={{
            direction: 'inherit',
          }}
          headerHeight={headerHeight}
          className={classes.table}
          rowCount={rowCount}
          rowGetter={rowGetter}
          rowClassName={getRowClassName}
        >
          {columns.map(({ dataKey, ...other }, index) => {
            return (
              <Column
                key={dataKey}
                headerRenderer={({ label }) => (
                  <VirtualizedTableHeader
                    columnIndex={index}
                    columns={columns}
                    label={label}
                    headerHeight={headerHeight}
                  />
                )}
                className={classes.flexContainer}
                cellRenderer={({ cellData, columnIndex }) => (
                  <VirtualizedTableCell
                    cellData={cellData}
                    columnIndex={columnIndex}
                    columns={columns}
                    rowHeight={rowHeight}
                    onRowClick={onRowClick}
                  />
                )}
                dataKey={dataKey}
                {...other}
              />
            );
          })}
        </Table>
      )}
    </AutoSizer>
  );
}
