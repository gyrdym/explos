import { TableCell } from '@material-ui/core';
import clsx from 'clsx';
import * as React from 'react';
import { IVirtualizedTableColumn } from './VirtualizedTable.interface';
import { useVirtualizedTableStyles } from './VirtualizedTable.style';

interface IVirtualizedTableHeader {
  label?: React.ReactNode;
  columnIndex: number;
  columns: IVirtualizedTableColumn[];
  headerHeight: number;
}

export function VirtualizedTableHeader({
  label,
  columnIndex,
  columns,
  headerHeight,
}: IVirtualizedTableHeader) {
  const classes = useVirtualizedTableStyles();

  return (
    <TableCell
      component="div"
      className={clsx(
        classes.tableCell,
        classes.flexContainer,
        classes.noClick,
      )}
      variant="head"
      style={{ height: headerHeight }}
      align={columns[columnIndex].numeric || false ? 'right' : 'left'}
    >
      <span>{label}</span>
    </TableCell>
  );
}
