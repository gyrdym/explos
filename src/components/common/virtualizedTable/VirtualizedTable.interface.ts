export interface IVirtualizedTableColumn<T extends string = string> {
  width: number;
  label: string;
  dataKey: T;
  numeric?: boolean;
}

export interface IVirtualizedTableProps<T> {
  onRowClick?: () => void;
  headerHeight?: number;
  rowHeight?: number;
  columns: IVirtualizedTableColumn[];
  rowCount: number;
  rowGetter: ({ index }: { index: number }) => T;
}
