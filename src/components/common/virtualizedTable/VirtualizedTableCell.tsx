import { TableCell } from '@material-ui/core';
import clsx from 'clsx';
import * as React from 'react';
import { IVirtualizedTableColumn } from './VirtualizedTable.interface';
import { useVirtualizedTableStyles } from './VirtualizedTable.style';

interface IVirtualizedTableCellProps {
  cellData?: React.ReactNode;
  columnIndex: number;
  columns: IVirtualizedTableColumn[];
  rowHeight: number;
  onRowClick?: () => void;
}

export function VirtualizedTableCell({
  cellData,
  columnIndex,
  columns,
  rowHeight,
  onRowClick,
}: IVirtualizedTableCellProps) {
  const classes = useVirtualizedTableStyles();

  return (
    <TableCell
      component="div"
      className={clsx(classes.tableCell, classes.flexContainer, {
        [classes.noClick]: Boolean(onRowClick),
      })}
      variant="body"
      style={{ height: rowHeight }}
      align={columns[columnIndex].numeric ? 'right' : 'left'}
    >
      {cellData}
    </TableCell>
  );
}
