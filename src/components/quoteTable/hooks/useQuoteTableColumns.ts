import { IVirtualizedTableColumn } from 'src/components/common/virtualizedTable/VirtualizedTable.interface';
import { IQuote } from 'src/interfaces/domain/quote';

export const useQuoteTableColumns = (): IVirtualizedTableColumn<
  keyof IQuote
>[] => {
  return [
    {
      width: 200,
      label: 'Symbol',
      dataKey: 'symbol',
    },
    {
      width: 120,
      label: 'Company name',
      dataKey: 'companyName',
    },
    {
      width: 120,
      label: 'Sector',
      dataKey: 'sector',
    },
    {
      width: 120,
      label: 'Latest price',
      dataKey: 'latestPrice',
      numeric: true,
    },
  ];
};
