import { Paper } from '@material-ui/core';
import * as React from 'react';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { loadQuotes } from '../../redux/quotes/quotes.actions';
import { quotesSelector } from '../../redux/quotes/quotes.selectors';
import { sectorsSelector } from '../../redux/sectors/sectors.selectors';
import { useAppDispatch } from '../../redux/store';
import { VirtualizedTable } from '../common/virtualizedTable/VirtualizedTable';
import { useQuoteTableColumns } from './hooks/useQuoteTableColumns';

export const QuoteTable = () => {
  const dispatch = useAppDispatch();
  const { selectedSectorName } = useSelector(sectorsSelector);
  const quotes = useSelector(quotesSelector);
  const columns = useQuoteTableColumns();

  useEffect(() => {
    dispatch(loadQuotes(selectedSectorName));
  }, [selectedSectorName]);

  return (
    <Paper style={{ height: 400, width: '100%' }}>
      <VirtualizedTable
        rowCount={quotes.length}
        rowGetter={({ index }) => quotes[index]}
        columns={columns}
      />
    </Paper>
  );
};
