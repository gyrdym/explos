import { default as React, useCallback, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { loadSectors } from '../../redux/sectors/sectors.actions';
import { sectorsSelector } from '../../redux/sectors/sectors.selectors';
import { sectorsSlice } from '../../redux/sectors/sectors.slice';
import { useAppDispatch } from '../../redux/store';
import { Dropdown } from '../common/dropdown/dropdown';

export function SectorMenu() {
  const dispatch = useAppDispatch();
  const { selectedSectorName, sectorNames } = useSelector(sectorsSelector);
  const handleClose = useCallback((name: string) => {
    dispatch(sectorsSlice.actions.selectSector(name));
  }, []);

  useEffect(() => {
    dispatch(loadSectors());
  }, []);

  return (
    <Dropdown
      selectedOption={selectedSectorName}
      placeholder={'Выбрать сектор'}
      onClose={handleClose}
      options={sectorNames}
    />
  );
}
