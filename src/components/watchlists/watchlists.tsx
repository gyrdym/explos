import {useSelector} from 'react-redux';
import {watchlistsSelector} from '../../redux/watchlists/watchlists.selector';
import {Watchlist} from '../watchlist/watchlist';
import {Button} from '@material-ui/core';
import {default as React, useState} from 'react';
import {AssetSearchForm} from '../assetSearchForm/assetSearchForm';

export function Watchlists() {
    const {watchlists} = useSelector(watchlistsSelector);
    const [isOpen, setIsOpen] = useState(false);
    const handleOpen = () => {
        setIsOpen(true);
    };
    const handleClose = () => {
        setIsOpen(false);
    };

    return (
        <div>
            {watchlists.length > 0
                ? watchlists.map((watchlist) => (<Watchlist />))
                : (<div>There are no watchlists</div>)}
            <Button
                variant="contained"
                color="primary"
                onClick={handleOpen}
            >
                Find asset
            </Button>
            <AssetSearchForm
                isOpen={isOpen}
                handleClose={handleClose}
            />
        </div>
    );
}
