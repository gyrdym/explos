import {iocTypes} from '../iocTypes';
import {IApi} from '../../interfaces/services/api';
import {Api} from '../../services/default/api';
import {ioc} from '../ioc';
import {Consola} from 'consola';
import {Logger} from '../../services/default/logger';

ioc
    .bind<IApi>(iocTypes.API)
    .to(Api);
ioc
    .bind<Consola>(iocTypes.LOGGER)
    .to(Logger);
